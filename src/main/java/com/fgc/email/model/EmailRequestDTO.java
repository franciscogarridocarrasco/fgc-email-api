package com.fgc.email.model;

import java.io.Serializable;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;

@Value
@Builder(toBuilder = true)
@EqualsAndHashCode
@ToString
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EmailRequestDTO implements Serializable {

    private String from;
    private String password;
    private String to;
    private String cc;
    private String bcc;
    private String subject;
    private String text;

}