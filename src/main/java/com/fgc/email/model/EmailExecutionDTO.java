package com.fgc.email.model;

import java.io.Serializable;
import lombok.Data;

@Data
public class EmailExecutionDTO implements Serializable {

    private EmailRequestDTO request;

    private EmailResponseDTO response;

}