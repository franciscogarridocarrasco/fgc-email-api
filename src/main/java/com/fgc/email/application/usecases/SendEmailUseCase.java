package com.fgc.email.application.usecases;

import static javax.mail.Message.RecipientType.BCC;
import static javax.mail.Message.RecipientType.CC;
import static javax.mail.Message.RecipientType.TO;

import com.fgc.email.model.EmailRequestDTO;
import com.fgc.email.util.AES256;
import java.util.Properties;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SendEmailUseCase {

  public String sendEmail(final EmailRequestDTO request) {
    final Properties properties = new Properties();
    properties.put("mail.smtp.auth", "true");
    properties.put("mail.smtp.socketFactory.port", 587);
    properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
    properties.put("mail.smtp.host", "smtp.ionos.es");
    properties.put("mail.smtp.port", 587);

    // Create session object passing properties and authenticator instance
    final Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
      protected PasswordAuthentication getPasswordAuthentication() {
        final String decrypt = AES256.decrypt(request.getPassword());
        return new PasswordAuthentication(request.getFrom(), decrypt);
      }
    });

    try {
      // Create MimeMessage object
      final MimeMessage message = new MimeMessage(session);

      // Set the Senders mail to From
      message.setFrom(new InternetAddress(request.getFrom()));

      // Set the recipients email address
      message.addRecipient(TO, new InternetAddress(request.getTo()));

      if (Strings.isNotEmpty(request.getCc())) {
        message.addRecipient(CC, new InternetAddress(request.getCc()));
      }

      if (Strings.isNotEmpty(request.getBcc())) {
        message.addRecipient(BCC, new InternetAddress(request.getBcc()));
      }

      // Subject of the email
      message.setSubject(request.getSubject());

      // Body of the email
      message.setContent(request.getText(), "text/html");

      // Send email.
      Transport.send(message);

      return "OK";
    } catch (MessagingException me) {
      log.error(me.getMessage(), me);
      return me.getMessage();
    } catch (Throwable e) {
      log.error(e.getMessage(), e);
      return e.getMessage();
    }
  }
}
