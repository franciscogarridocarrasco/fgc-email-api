package com.fgc.email.infrastructure.controller.rest;

import com.fgc.email.application.usecases.SendEmailUseCase;
import com.fgc.email.model.EmailRequestDTO;
import com.fgc.email.util.AES256;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
@RequestMapping("/api/v1/email")
@RequiredArgsConstructor
public class EmailSslReactiveRestController {

  private final SendEmailUseCase sendEmailUseCase;

  @GetMapping(value = "/encrypt/{encrypt}", produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<String> encrypt(@PathVariable("encrypt") final String encrypt) {
    return Mono.just(AES256.encrypt(encrypt));
  }

  @GetMapping(value = "/example", produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<EmailRequestDTO> emailExample() {
    return Mono.just(EmailRequestDTO.builder().build());
  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  public Mono<String> email(@RequestBody final EmailRequestDTO emailRequestDTO) {
    final String response = this.sendEmailUseCase.sendEmail(emailRequestDTO);
    return Mono.just(response);
  }

}
