package com.fgc.email.infrastructure.controller.mapper;

import com.fgc.projects.grpc.email.EmailRequest;

public class EmailMother {

  public static EmailRequest oneCompleteEmailRequest() {
    return EmailRequest.newBuilder().setFrom("from").setPassword("password").setTo("to").setCc("cc").setBcc("bcc").setSubject("subject")
        .setText("text").build();
  }

}
