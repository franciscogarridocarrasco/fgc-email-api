package com.fgc.email.infrastructure.controller.grpc;

import com.fgc.email.application.usecases.SendEmailUseCase;
import com.fgc.email.infrastructure.controller.mapper.GrpcEmailMapper;
import com.fgc.projects.grpc.email.EmailRequest;
import com.fgc.projects.grpc.email.EmailResponse;
import com.fgc.projects.grpc.services.EmailServiceEndPointGrpc.EmailServiceEndPointImplBase;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;

@Slf4j
@GRpcService
@RequiredArgsConstructor
public class EmailSslGrpcController extends EmailServiceEndPointImplBase {

  private final SendEmailUseCase sendEmailUseCase;

  private final GrpcEmailMapper grpcEmailMapper;

  @Override
  public void send(final EmailRequest request, final StreamObserver<EmailResponse> responseObserver) {
    this.log.debug("Sending email: {}", request);

    final String message = this.sendEmailUseCase.sendEmail(this.grpcEmailMapper.toEmailDTO(request));

    responseObserver.onNext(EmailResponse.newBuilder().setMessage(message).build());

    responseObserver.onCompleted();
  }

}

