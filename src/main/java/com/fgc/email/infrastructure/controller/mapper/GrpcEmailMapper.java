package com.fgc.email.infrastructure.controller.mapper;

import com.fgc.email.model.EmailRequestDTO;
import com.fgc.projects.grpc.email.EmailRequest;
import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
    componentModel = "spring")
public interface GrpcEmailMapper {

  EmailRequestDTO toEmailDTO(final EmailRequest emailRequest);

}
