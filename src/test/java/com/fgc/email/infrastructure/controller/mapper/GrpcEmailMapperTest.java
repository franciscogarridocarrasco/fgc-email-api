package com.fgc.email.infrastructure.controller.mapper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import com.fgc.email.model.EmailRequestDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GrpcEmailMapperTest {

  private GrpcEmailMapper grpcEmailMapper;

  @BeforeEach
  public void setUp() {
    this.grpcEmailMapper = new GrpcEmailMapperImpl();
  }

  @Test
  void givenAEmailRequestWhenInvokeToEmailRequestDTOThenReturnEmailRequestDTO() {
    final var emailRequest = EmailMother.oneCompleteEmailRequest();

    final EmailRequestDTO emailRequestDTO = this.grpcEmailMapper.toEmailDTO(emailRequest);

    assertThat(emailRequestDTO, is(notNullValue()));
    assertThat(emailRequestDTO.getFrom(), is("from"));
    assertThat(emailRequestDTO.getPassword(), is("password"));
    assertThat(emailRequestDTO.getTo(), is("to"));
    assertThat(emailRequestDTO.getCc(), is("cc"));
    assertThat(emailRequestDTO.getBcc(), is("bcc"));
    assertThat(emailRequestDTO.getSubject(), is("subject"));
    assertThat(emailRequestDTO.getText(), is("text"));
  }

}